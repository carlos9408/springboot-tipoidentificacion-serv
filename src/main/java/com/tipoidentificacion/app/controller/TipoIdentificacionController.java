package com.tipoidentificacion.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tipoidentificacion.app.dto.rs.UsuarioRS;
import com.tipoidentificacion.app.entity.TipoIdentificacion;
import com.tipoidentificacion.app.service.ITipoIdentificacionService;

@RestController
@RequestMapping
public class TipoIdentificacionController {

	@Autowired
	private ITipoIdentificacionService tipoIdentificacionService;

	@GetMapping("/all")
	public List<TipoIdentificacion> getAll() {
		return tipoIdentificacionService.getAll();
	}

	@GetMapping("/find/usuarios/by/{idTipoIdentificacion}")
	public List<UsuarioRS> getAll(@PathVariable(value = "idTipoIdentificacion") final Long idTipoIdentificacion) {
		return tipoIdentificacionService.getUsuariosByTipoIdentificacion(idTipoIdentificacion);
	}

	//TODO find by id (Estallarse)

	//TODO Fallback (Return TipoIdentificacion por defecto)

}