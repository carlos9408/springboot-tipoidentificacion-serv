package com.tipoidentificacion.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tipoidentificacion.app.dto.rs.UsuarioRS;
import com.tipoidentificacion.app.entity.TipoIdentificacion;
import com.tipoidentificacion.app.util.Util;

@Repository
public interface TipoIdentificacionRepository extends JpaRepository<TipoIdentificacion, Long> {

	@Query(value = "SELECT " + Util.DTO_PACKAGE
			+ "UsuarioRS(u.id, u.nombre, ti.codigo) FROM Usuario u JOIN u.tipoIdentificacion ti WHERE u.id =:idTipoIdentificacion")
	List<UsuarioRS> getUsuariosByTipoIdentificacion(@Param("idTipoIdentificacion") final Long idTipoIdentificacion);

}
