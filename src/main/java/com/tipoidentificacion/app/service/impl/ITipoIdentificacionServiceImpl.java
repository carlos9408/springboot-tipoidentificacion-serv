package com.tipoidentificacion.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tipoidentificacion.app.dto.rs.UsuarioRS;
import com.tipoidentificacion.app.entity.TipoIdentificacion;
import com.tipoidentificacion.app.repository.TipoIdentificacionRepository;
import com.tipoidentificacion.app.service.ITipoIdentificacionService;
import com.tipoidentificacion.app.util.Util;

@Service
@Transactional
@Primary
public class ITipoIdentificacionServiceImpl implements ITipoIdentificacionService {

	@Autowired
	private TipoIdentificacionRepository tipoIdentificacionRepo;

	@Autowired
	private Environment env;

	@Override
	@Transactional(readOnly = true)
	public List<TipoIdentificacion> getAll() {
		return tipoIdentificacionRepo.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<UsuarioRS> getUsuariosByTipoIdentificacion(final Long idTipoIdentificacion) {
		final List<UsuarioRS> response = tipoIdentificacionRepo.getUsuariosByTipoIdentificacion(idTipoIdentificacion);
		response.forEach(rs -> rs.setLocalPort(env.getProperty(Util.SERVER_PORT)));
		return response;
	}

	public TipoIdentificacion getById(final Long id) {
		//TODO enviar un id null
		//TODO buscar un id que no existe (si devuelve null intenten acceder a una propiedad desde el controller)
		return tipoIdentificacionRepo.getById(id);
	}

}
