package com.tipoidentificacion.app.service;

import java.util.List;

import com.tipoidentificacion.app.dto.rs.UsuarioRS;
import com.tipoidentificacion.app.entity.TipoIdentificacion;

public interface ITipoIdentificacionService {

	List<TipoIdentificacion> getAll();

	List<UsuarioRS> getUsuariosByTipoIdentificacion(final Long idTipoIdentificacion);
}
